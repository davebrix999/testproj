# App level

from django.urls import path
from .views import Screen

urlpatterns=[
	path("", Screen.as_view(), name="home"),
]